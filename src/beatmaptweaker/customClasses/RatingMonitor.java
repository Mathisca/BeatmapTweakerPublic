package beatmaptweaker.customClasses;

import beatmaptweaker.menus.MultipleRate;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class RatingMonitor implements Runnable {

    private final MultipleRate sourceFrame;
    
    public RatingMonitor(MultipleRate sourceFrame) {
        this.sourceFrame = sourceFrame;
    }
    
    @Override
    public void run() {
            Runnable r = new StartRating(this.sourceFrame);
            Thread th = new Thread(r);
            th.start();
            try {
                th.join(10000);
                if (th.isAlive()) {
                    JOptionPane.showMessageDialog(null, "This audio file is not supported yet - Only the beatmap will be generated.");
                    th.stop();
                    this.sourceFrame.cleanBeforeForce();
                    this.sourceFrame.startRating();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(MultipleRate.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

}