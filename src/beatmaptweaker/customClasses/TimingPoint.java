/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beatmaptweaker.customClasses;

/**
 *
 * @author ADRIEN
 */
public class TimingPoint {
    private int msOffset;
    private float speed;
    private int type;
    private int _unk1;
    private int _unk2;
    private int _unk3;
    private int _unk4;
    private int _unk5;
    
    public TimingPoint(String line) {
        String[] data = line.split(",");
        
        if(line.split(",").length==8) {
            this.msOffset = Math.round(Float.valueOf(data[0]));
            this.speed = Float.valueOf(data[1]);
            this.type = Integer.valueOf(data[2]);
            
            this._unk1 = Integer.valueOf(data[3]);
            this._unk2 = Integer.valueOf(data[4]);
            this._unk3 = Integer.valueOf(data[5]);
            this._unk4 = Integer.valueOf(data[6]);
            this._unk5 = Integer.valueOf(data[7]);
        }
    }
    
    public void rateOffset(float rate, int decal) {
        this.setMsOffset((int) ((this.getMsOffset() + decal) / rate));
        if (this.getSpeed()>0) {
        this.setSpeed(this.getSpeed() / rate);
        }
    }
    
    @Override
    public String toString() {
        return Integer.toString(msOffset) + "," 
                + Float.toString(speed) + "," 
                + Integer.toString(type) + "," 
                + Integer.toString(_unk1) + "," 
                + Integer.toString(_unk2) + "," 
                + Integer.toString(_unk3) + "," 
                + Integer.toString(_unk4) + "," 
                + Integer.toString(_unk5);
    }

    public int getMsOffset() {
        return msOffset;
    }

    public void setMsOffset(int msOffset) {
        this.msOffset = msOffset;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int get1() {
        return _unk1;
    }

    public void set1(int _unk1) {
        this._unk1 = _unk1;
    }

    public int get2() {
        return _unk2;
    }

    public void set2(int _unk2) {
        this._unk2 = _unk2;
    }

    public int get3() {
        return _unk3;
    }

    public void set3(int _unk3) {
        this._unk3 = _unk3;
    }

    public int get4() {
        return _unk4;
    }

    public void set4(int _unk4) {
        this._unk4 = _unk4;
    }

    public int get5() {
        return _unk5;
    }

    public void set5(int _unk5) {
        this._unk5 = _unk5;
    }
    
    
}
