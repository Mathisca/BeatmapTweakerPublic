package beatmaptweaker;

import beatmaptweaker.customClasses.Beatmap;
import beatmaptweaker.menus.BeatmapDiffs;
import beatmaptweaker.menus.BeatmapSelection;
import beatmaptweaker.menus.MultipleRate;
import beatmaptweaker.menus.Welcome;
import java.io.File;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author adrien
 */
public class BeatmapTweaker {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        
        String version = "b0.4.1";
        int fluid = 20;

        boolean isRunning = true;

        Welcome welcome = new Welcome();
        welcome.show();

        welcome.setVisible(false);
        welcome.dispose();

        while (isRunning) {

            BeatmapSelection bmS = new BeatmapSelection(version);
            bmS.show();

            while (!bmS.isDone()) {
                sleep(fluid);
            }

            String path = bmS.getSelectedPath();

            ArrayList<Beatmap> selectedBeatmaps = new ArrayList<Beatmap>();
            File[] files = new File(path).listFiles();

            for (File file : files) {
                if ("osu".equals(Utils.getFileExtension(file))) {
                    selectedBeatmaps.add(new Beatmap(file.getName(), (ArrayList) Files.readAllLines(file.toPath(), StandardCharsets.UTF_8)));
                }
            }

            String currentDir = bmS.getActiveDirectory();

            bmS.setVisible(false);
            bmS.dispose();

            boolean selectingDiff = true;

            while (selectingDiff) {
                BeatmapDiffs selectDiffs = new BeatmapDiffs(selectedBeatmaps, currentDir);
                selectDiffs.show();

                while (selectDiffs.getState() == 0) {
                    sleep(fluid);
                }

                if (selectDiffs.getState() == 2) {
                    selectDiffs.setVisible(false);
                    ArrayList<Beatmap> selectedDifficulties = selectDiffs.getSelectedBeatmaps();

                    if (selectedDifficulties.size() >= 1) {
                        MultipleRate rate = new MultipleRate(selectedDifficulties, currentDir);
                        rate.show();

                        while (rate.getState() == 0) {
                            sleep(fluid);
                        }

                        if (rate.getState() == 1) {
                            selectDiffs.setState(0);
                            rate.setVisible(false);
                        }
                    }

                } else {
                    selectingDiff = false;
                    selectDiffs.dispose();
                }
            }
        }
    }

}
